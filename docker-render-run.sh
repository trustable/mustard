#!/bin/bash
#
# Copyright (C) 2020 Codethink Limited
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

MUSTARDPATH=$PWD
PORT="8080"

# Approach taken from:
#  https://stackoverflow.com/questions/192249/how-do-i-parse-command-line-arguments-in-bash

POSITIONAL=()
while [[ $# -gt 0 ]]; do
  key="$1"

  case $key in
      -m|--mustard-path)
      MUSTARDPATH="$2"
      shift
      shift
      ;;
      -p|--port)
      PORT="$2"
      shift
      shift
      ;;
  esac
done
set -- "${POSITIONAL[@]}" # restore positional parameters

echo "MUSTARD REPO = ${MUSTARDPATH}"
echo "PORT         = ${PORT}"

docker run -p $PORT:8080 -d -it --name mustard-render --mount type=bind,source=$MUSTARDPATH,target="/srv/mustard/mustard/mustard-mount/" mustard:1.0
