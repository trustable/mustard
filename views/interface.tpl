% if detail == 'list':
  % if interface:
    <a class="interface" href="{{interface.tree.state.url}}/interfaces#{{path}}">{{interface.title}}
      {{!'<span class="error">☐</span>' if not interface.implemented_by else '☑'}} <span>{{path}}</span>
    </a>
  % else:
    % include pathnotfound path=path, detail=detail
  % end
% elif detail == 'full':
  % if interface:
    <dt><h2 id="{{path}}">{{!'<span class="error">☐</span>' if not interface.implemented_by else '☑'}}{{interface.title}} <span><a href="#{{path}}" onclick="return false">{{path}}</a></span></h2></dt>
    <dd>
      <table cellspacing="0" cellpadding="0">
        <tr>
          <th>Description</th>
          <td>{{!interface.description}}</td>
        </tr>
        % if interface.implemented_by:
          <tr>
            <th>Implemented by</th>
            <td>
              <ul class="list">
                % for path, comp in interface.implemented_by.items():
                  <li>
                    % include component path=path, component=comp, detail='list'
                  </li>
                % end
              </ul>
            </td>
          </tr>
        % end
        % if interface.required_by:
          <tr>
            <th>Required by</th>
            <td>
              <ul class="list">
                % for path, comp in interface.required_by.items():
                  <li>
                    % include component path=path, component=comp, detail='list'
                  </li>
                % end
              </ul>
            </td>
          </tr>
        % end
        % include references element=interface
        % include tags-list element=interface
        % include parents-list element=interface
        % include requirements-list element=interface
        % include work-items-list element=interface
        % include verification-criterion-list element=interface
      </table>
    </dd>
  % else:
    % include pathnotfound path=path, detail=detail
  % end
% end
