Mustard README
==============

This is the official repository for the development of Mustard,
the Mapped Uniform System for Tracking Architecture, Requirements
and Design.

## Contents

1. Screenshots
2. Mustard data format
3. Creating UML diagrams
4. Converters
5. Installing and running Mustard
6. Contributing
7. Copyright and License

## Screenshots

### Requirements

![Requirements](https://gitlab.com/trustable/mustard/raw/master/data/screenshots/requirements.png "Requirements")

### Architecture

![Architecture](https://gitlab.com/trustable/mustard/raw/master/data/screenshots/architecture.png "Architecture")

### Work items

![Work Items](https://gitlab.com/trustable/mustard/raw/master/data/screenshots/work-items.png "Work Items")

### History

![History](https://gitlab.com/trustable/mustard/raw/master/data/screenshots/history.png "History")

![Diffs](https://gitlab.com/trustable/mustard/raw/master/data/screenshots/diff.png "Diffs")


## Mustard data format

### General concepts

Mustard allows you to manage the following information about a project:

  * Requirements
  * Components / Architectures
  * Interfaces
  * Integration strategies
  * Verification criteria
  * Work Items

Mustard reads these elements from YAML files stored in a Git repository.
Every commit in the repository represents a different state of the
overall architecture.

Each element is specified as dictionary with a special key called `kind`
that identifies the type of the element.

Two examples:

    kind: requirement
    title: The software needs to do XYZ

And:

    kind: component
    title: Web application
    description: Some Markdown-formatted text to describe the component.
    parent: components/software

Every element in the project is associated with a unique identifier that
is very similar to a path in a file system. The identifiers allow to
link elements together and thereby establish a bidirectional mapping
from requirements all the way down to work items and back.

How the YAML files are organised in the repository tree is up to the
architects. The element paths are specified as follows:

### Element identifiers

One possible way to store elements in the YAML repository is to use
one file per element. In this case the path to the file, relative to
the repository directory and without the .yaml extension, becomes the
identifier of the element.

For example, a file called

    $repo/requirements/software/feature-xyz.yaml

with the contents

    kind: requirements
    title: Feature XYZ

would result in a requirement element with the identifier
`requirements/software/feature-xyz`.

In addition to this, nested dictionaries in a YAML file can be used as
an effective way to group elements. In this case, the path to the YAML
file plus the hierarchy of dictionary keys leading to the elements
become the element identifiers.

First example: a file called

    $repo/components/software.yaml

with the contents

    webapp:
      kind: component
      title: Web application
      parent: components/software

    phoneapp:
      kind: component
      title: Phone app
      parent: components/software

results in two component elements with the identifiers
`components/software/webapp` and `components/software/phoneapp`.

A file called

    $repo/requirements.yaml

with nested dictionaries

    system:
      software:
        rails:
          kind: requirement
          title: Web application written in Ruby on Rails
      infrastructure:
        heroku:
          kind: requirement
          title: Web app deployment via Heroku

will generate two requirement elements with the identifiers
`requirements/system/software/rails` and
`requirements/system/infrastructure/heroku`.

It is also possible to nest elements like this:

    requirements.yaml:

    system:
      software:
        webapp:
          kind: requirement
          title: There needs to be a web application

          heroku:
            kind: requirement
            title: Web app deployment via Heroku
            parent: requirements/system/software/webapp

The above results in two elements with the identifiers
`requirements/system/software/webapp` and
`requirements/system/software/webapp/heroku`.

### Uniqueness

NOTE: Mustard requires that all identifiers in the project be unique.
It will raise an error whenever there are duplicates in the system and
will try to help you resolve them.

### References

Mustard descriptions are, as stated above, Markdown.  In order to make the YAML
slightly more readable, we recommend using reference links of the form:

    description: |
      This is a reference [link][]

      [link]: https://daringfireball.net/projects/markdown/syntax#link

In order to make this even more useful, you can break your reference links out
into a separate dictionary in the form:

    description: |
      This is a reference [link][]

    references:
      link: https://daringfireball.net/projects/markdown/syntax#link

In addition to satisfying the Markdown reference link, the references will be
rendered on the web UI as separated out links to help users understand what is
going on.  (The exception to this is the project element whose `references:` links
are not rendered separately).  You can, as you'd expect, use renamed reference
links:


    description: |
      This is a [reference link][renamed]

    references:
      renamed: https://daringfireball.net/projects/markdown/syntax#link

### Supported elements and keys

Project (kind: project):

  Used to state core project configuration.  Entirely optional, but no more
  than one may exist.

  Supported keys:

    - title       (optional)
    - description (optional)
    - references  (optional)
    - copyright   (optional)
    - sort-by     (optional)

  Example:

    kind: project
    title: The FooBar project
    copyright: 2013 Badger Corporation
    sort-by: location
    description: A string in Markdown format.

Requirements (kind: requirement, req, r):

  Used to specify requirements.

  Supported keys:

    - title       (optional)
    - description (optional)
    - references  (optional)
    - tags        (optional)
    - parent      (optional)

  Example:

    kind: requirement
    title: Any string
    description: A string in Markdown format.
    tags:
      - tags/required
      - tags/high-priority
    parent: identifier/of/parent/requirement

Components (kind: component, comp, c):

  Used to specify any components in the system. The result is a
  hierarchy of components also called the "architecture".

  Supported keys:

    - title       (optional)
    - description (optional)
    - references  (optional)
    - tags        (optional)
    - parent      (optional)
    - mapped-here (optional)
    - exposes     (optional)
    - requires    (optional)

  Example:

    kind: component
    title: Any string
    description: A string in Markdown format.
    tags:
      - tags/foo
      - tags/bar
    parent: identifier/of/parent/component
    mapped-here:
      - identifier/of/a/requirement
      - identifier/of/another/requirement
    exposes:
      - identifier/of/an/interface
    requires:
      - identifier/of/a/different/interface

Interfaces (kind: interface, iface, i):

  Used to specify interfaces of components.

  Supported keys:

    - title       (optional)
    - description (optional)
    - references  (optional)
    - tags        (optional)
    - parent      (optional)
    - mapped-here (optional)

  Example:

    kind: interface
    title: Any string
    description: A string in Markdown format.
    tags:
      - tags/foo
      - tags/bar
    parent: identifier/of/parent/component
    mapped-here:
      - identifier/of/a/requirement
      - identifier/of/another/requirement

Integration strategies (kind: integration-strategy, istrat, s):

  Used to specify integration strategies for components.

  Supported keys:

    - title       (optional)
    - description (optional)
    - references  (optional)
    - tags        (optional)
    - parent      (optional)
    - mapped-here (optional)

  Example:

    kind: integration-strategy
    title: Any string
    description: A string in Markdown format.
    tags:
      - tags/foo
      - tags/bar
    parent: identifier/of/parent/component
    mapped-here:
      - identifier/of/a/requirement
      - identifier/of/another/requirement

Verification Criterion (kind: verification-criterion, vcrit, v):

  Used to describe how to verify that an an integration strategy, a component
  or an interface is performed or implemented properly; or how to know if a
  requirement has been met.

  Supported keys:

    - title       (optional)
    - description (optional)
    - references  (optional)
    - tags        (optional)
    - parent      (optional)
    - mapped-here (optional)

  Example:

    kind: verification-criterion
    title: Any string
    description: A string in Markdown format.
    tags:
      - tags/foo
      - tags/bar
    parent: identifier/of/an/integration-strategy
    mapped-here:
      - identifier/of/a/requirement
      - identifier/of/another/requirement

Work Items (kind: work-item, work, w):

  Used to specify work items required to implement/realise components,
  interfaces, tests etc., all in order to satisfy the system
  requirements.

  Supported keys:

    - title       (optional)
    - description (optional)
    - references  (optional)
    - tags        (optional)
    - parents     (optional)
    - mapped-here (optional)

  Example:

    kind: work-item
    title: Any string
    description: A string in Markdown format.
    tags:
      - tags/foo
      - tags/bar
    parents:
      - identifier/of/an/component
      - identifier/of/a/component
      - identifier/of/an/interface
      - identifier/of/an/integration-strategy
      - identifier/of/a/verification-criterion
    mapped-here:
      - identifier/of/a/requirement
      - identifier/of/another/requirement

Tags (kind: tag, t)

  Used to provide generic tagging of other nodes in the Mustard.

  Supported keys:

    - title       (optional)
    - description (optional)
    - references  (optional)

  Example:

    kind: tag
    title: "Attention: Architect"
    description: |
      The architect needs to think here, it's not ready.


## Creating UML diagrams

All elements in a Mustard repository can include UML diagrams in their
`description` field. Mustard is using PlantUML to parse the descriptions
of these diagrams and generate images that can be embedded in the web
interface or printed output.

### Defining a UML diagram in an element

Descriptions of Mustard elements are Markdown. Mustard, however, extends
the Markdown format by UML blocks opened with `@startuml` and closed with
`@enduml`. The content inside these blocks has to be PlantUML syntax.

An example interface with a simple sequence diagram could look as
follows:

    kind: interface
    title: Public service interface
    description: |
      The public interface of the `Foo` service.

      @startuml
      Client -> Service : subscribe()
      Service -> Client : subscribed()
      @enduml

### The PlantUML syntax

For the syntax and supported variants of UML diagrams, please refer to
the PlantUML website, which provides an extensive list of examples:

  http://plantuml.sourceforge.net/

## Converters

Mustard has tooling to convert from two common enterprise data formats.  The
conversions are meant to support ongoing import; but are not bidirectional.

### Enterprise Architect

The `ea-import` tool is used to import Enterprise Architect XML/XMI exports
into Mustard.  If you run `ea-import --help` you'll see how to control it,
though the critical arguments are:

* `-o path/to/mustard`
* `-i path/to/ea-export.xml`

The `ea-import` tool can convert requirements, components, and interfaces; and
will do its best to link everything up and draw UML diagrams where possible.

### DOORS

The `doors-import` tool is used to import combinations of IBM Rational DOORS
modules into Mustard.  In order to use `doors-import` you need to write a file
which instructs `doors-import` in how to process each module which forms your
set of requirements.  These instructions are in the form of a YAML file which
is a list of modules.

    - module: feature
      file: features.CSV
      name:
        kind: field
        field: "Object Number"
      matches: |
        lambda e,n: (e['Object Text'] + " .").split()[0] == n
      fields:
        - name: parent
          kind: maybelink
          lambda: |
            lambda e, i: i.find_by('feature', 'Object Number',
                  e['Object Number'].rsplit("-", 1)[0] if "-" in e['Object Number'] else None)
        - name: title
          kind: plain
          lambda: |
            lambda e: e['Object Text'] + " - " + e.get('Feature Title', '')
        - name: description
          kind: plain
          lambda: |
            lambda e: e['Feature Description']
        - name: description
          kind: collate
          sort: True
          ignore:
            - 'Object Text'
            - 'Feature Title'
            - 'Feature Description'
          
    - module: antennas
      file: antennas.CSV
      name:
        kind: field
        field: "Object Identifier"
      fields:
        - name: parent
          kind: maybelink
          lambda: |
            lambda e, i: i.find_by('antennas', 'Object Number',
                e['Object Number'].rsplit(".", 1)[0] if "." in e['Object Number'] else None)
        - name: title
          kind: plain
          lambda: |
            lambda e: e['Object Short Text']
        - name: description
          kind: plain
          lambda: |
            lambda e: e['Object Text']
        - name: references
          kind: internalrefs
          lambda: |
            lambda e: [d.strip() for d in e.get('Feature-Item', '').split(",")]
        - name: description
          kind: collate
          sort: True
          ignore:
            - 'Object Short Text'
            - 'Object Text'
            - 'Object Identifier'
            - 'Feature-Item'

* `module` is the module name and will form part of the mustard element name.
* `file` is the filename of the module's CSV export.  This is relative to the
  YAML file
* `name` is the instruction to `doors-import` for how to construct the element
  name for the given row.  This needs to be unique within the module.
* `fields` contains the instructions to `doors-import` for how to map columns
  from the CSV into Mustard fields.  Important fields include the `parent`,
  `title`, `description`, and `references`.  You can, in addition to a plain
  field, append a collation of columns by adding a `collate` field of the same
  name afterwards.

It's worth noting that the DOORS importer requires that the modules understand
at least part of the internal API of `doors-import` and as such they are more
fragile than `ea-import` datasets.

## Installing and running Mustard

### Using Vagrant

If you have VirtualBox and vagrant, just run `vagrant up` to create a new VM from the supplied Vagrantfile.

The VM will be created with two directories mounted to your host system:

- /src/mustard containing the mustard checkout itself
- /src/mustard-example containing a checkout of some example mustard content 

The Vagrantfile launches mustard to serve the sample content at port 8080 of the default ip address for the VM.

### Local Installation

Mustard can be run locally from its source directory, after running the provided ./install_depencencies.sh script.

    ./mustard-render -b -r -j ./plantuml.jar -p /path/to/mustard/to/render

This method serves content at http://0.0.0.0:8080 by default. It should NOT be used for deployment.

The usual way to run a mustard service is to clone the source code repository
and integrate it with Apache or any WSGI-compatible web server. See the
following  section for more details on how to deploy Mustard using Apache.


#### Dependencies

Mustard requires the following:

  * Python >= 3.7.5
  * PyYAML
  * python3-markdown
  * python3-bottle
  * python3-pygit2 (and libgit2)
  * cliapp

Most of the dependencies should be available in your distro, apart from
libgit2, pygit2 and cliapp. The section below explains how to install these
easily.

#### Installing pygit2

If you lack pygit2 in your distribution, the safest way to get it is to
prepare a statically built libgit2 in a temporary installation location
and then link pygit2 against that.  Since libgit2 is not API/ABI stable
yet, this will innoculate you against others installing different
versions of libgit2 at other times.  It won't save you from incompatible
pygit2 installs, but you can fiddle with python paths if you care about
that.

To build libgit2 statically and pygit2 with that, do the following:

    $ mkdir pygit-building
    $ cd pygit-building
    $ git clone git://github.com/libgit2/libgit2.git
    $ git clone git://github.com/libgit2/pygit2.git
    $ cd libgit2
    $ git checkout v0.23.4
    $ mkdir build
    $ cd build
    $ cmake -DBUILD_SHARED_LIBS:BOOLEAN=OFF \
        -DCMAKE_INSTALL_PREFIX=$(pwd)/install ..
    $ make
    $ make install DESTDIR=""
    $ cd ../../pygit2
    $ git checkout v0.23.3
    $ LIBGIT2="$(pwd)/../libgit2/build/install" LIBGIT2="$(pwd)/../libgit2/build/install" \
      LDFLAGS="-Wl,-rpath='$LIBGIT2/lib',--enable-new-dtags $LDFLAGS" python setup.py build
    $ python setup.py install --user

This will install pygit2 into $HOME/.local -- obviously you can tweak the
`setup.py` invocations if you wish to alter where it installs to.

#### Installing cliapp

If not available via your distro, cliapp can be installed using the
build_cliapp.sh script included at the top level of this repository.

### Deploying Mustard using Apache2

Mustard can be set up as an Apache site easily using mod_wsgi. It ships
an `adapter.wsgi` file that can be set up to handle HTTP requests as
follows:

  <VirtualHost *:80>
      ServerName someserver.org
      ServerAdmin root
      DocumentRoot /var/www/someserver.org

      WSGIPassAuthorization On
      WSGIDaemonProcess mustard user=www-data group=www-data \
        processes=1 threads=5
      WSGIProcessGroup mustard
      WSGIScriptAlias / /var/www/someserver.com/adapter.wsgi
      WSGIApplicationGroup %{GLOBAL}

      <Directory /var/www/someserver.com>
        SetEnv MUSTARD_CONFIG_FILE /path/to/mustard.conf
        SetEnv MUSTARD_AUTH none
        SetEnv MUSTARD_SERVER_PATH /var/www/someserver.com/
        SetEnv MUSTARD_PROJECT_PATH /path/to/the/mustard-repo.git
        SetEnv MUSTARD_PLANTUML_JAR /path/to/plantuml.jar

        Order deny,allow
        Allow from all
      </Directory>
  </VirtualHost>

For the above to work, Mustard, or at least its `adapter.wsgi` must be
located in `/var/www/someserver.com`. The source tree with
`adapter.wsgi` may also be located somewhere outside `DocumentRoot`.

#### Automatically updating from upstream repositories

Mustard can automatically update the source repository it uses from
its remotes. To enable this, use the command-line option "--auto-fetch" to
'mustard-render', or the 'SetEnv MUSTARD_AUTOFETCH 1' directive in an Apache
WSGI configuration.

For this to work, you'll need to use a mirrored, bare git repository as the
project repository. You can clone one of these with:

    git clone --mirror <url>

You should clone using a method that does not require authentication, such
as HTTP/HTTPS, or ssh/git protocol with a authentication mechanism
available to your www user without a password.

You can have several remotes with different transports - as long as one
works, it will be fetched.

Fetching from remote repositories will happen as soon as a page is served,
but it is asynchronous, so you may need to perform two refreshes to see an
update.

## Contributing

Mustard is a Codethink project as part of the [Trustable](https://trustable.io)
initiative. Its development takes place within the Trustable project on GitLab:

  https://gitlab.com/trustable/mustard

Anyone interested in improving Mustard is welcome to clone the project
repository and send pull requests.

## Codebase

An overview of the Mustard codebase:

[![Mustard's codebase](/data/mustard-codebase.svg.png)](/data/mustard-codebase.svg)

(At the time of writing GitLab can be finnicky about whether to display
SVG files either as an image or as XML text. So rather than linking
directly to the SVG file, to get a legible image when you've clicked
through to the SVG you will need to select the 'View Image' item,
or its equivalent in your browser, from the image's context menu.) 

## Copyright & License

Copyright (C) 2012-2018 Codethink Ltd.

Mustard is licensed under the GNU Affero General Public License
Version 3, (AGPLv3). The full text of the license can be found
in the COPYING file distributed along with this README.

Mustard ships a copy of jQuery in views/public/, which is licensed
under the MIT license (see COPYING.jquery for more information about
the license).

Musta also ships a copy of PlantUML, which is licensed under GPLv3. See
http://plantuml.com/faq for information about this and other license
options.
