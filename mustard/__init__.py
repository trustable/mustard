#!/usr/bin/python3
#
# Copyright (C) 2012-2013, 2020 Codethink Limited
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


"""The Mustard library."""


from . import authenticator
from . import elementfactory
from . import sorting
from . import component
from . import integration
from . import interface
from . import project
from . import requirement
from . import tag
from . import criterion
from . import workitem
from . import renderer
from . import elementtree
from . import rawtree
from . import repository
from . import state
from . import util

from . import auth


class MustardError(Exception):
    """Errors directly associated with MUSTARD."""
