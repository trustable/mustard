#!/usr/bin/python3
#
# Copyright (C) 2012-2013, 2020 Codethink Limited
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from itertools import groupby


def comparator(sort_key):
    def compare_elements(pair1, pair2):
        def cmp(left, right):
            return (left > right) - (left < right)

        def split_by_numbers(s_n):
            return ["".join(v) for _, v in groupby(s_n, lambda c: c.isdigit())]

        s_1 = getattr(pair1[1], sort_key) if pair1 and pair1[1] else ""
        s_2 = getattr(pair2[1], sort_key) if pair2 and pair2[1] else ""

        s1s = split_by_numbers(s_1)
        s2s = split_by_numbers(s_2)

        s1s.append("")
        s2s.append("")

        for left, right in zip(s1s, s2s):
            leftdigit = left.isdigit()
            rightdigit = right.isdigit()
            if leftdigit and rightdigit:
                cmpres = cmp(int(left), int(right))
                if cmpres != 0:
                    return cmpres
            elif leftdigit:
                return 1 if right == "" else -1
            elif rightdigit:
                return -1 if left == "" else 1
            cmpres = cmp(left, right)
            if cmpres != 0:
                return cmpres
        return 0

    return compare_elements


def cmp_to_key(sort_by):
    "Convert a cmp= function into a key= function"

    class K(): # pylint: disable=invalid-name, unused-argument
        def __init__(self, obj, *args):
            self.obj = obj

        def __lt__(self, other):
            return sort_by(self.obj, other.obj) < 0

        def __gt__(self, other):
            return sort_by(self.obj, other.obj) > 0

        def __eq__(self, other):
            return sort_by(self.obj, other.obj) == 0

        def __le__(self, other):
            return sort_by(self.obj, other.obj) <= 0

        def __ge__(self, other):
            return sort_by(self.obj, other.obj) >= 0

        def __ne__(self, other):
            return sort_by(self.obj, other.obj) != 0

    return K


def sort_elements(elements, args):
    real_elements = [e for p, e in elements if e is not None]
    if real_elements:
        default_sort_by = real_elements[0].tree.project.sort_by
    else:
        default_sort_by = None
    sort_by = args.get("sort_by", None)
    if sort_by == "DEFAULT":
        sort_by = default_sort_by
    reverse = args.get("reverse", False)
    if sort_by:
        return sorted(elements, key=cmp_to_key(comparator(sort_by)), reverse=reverse)

    return elements
