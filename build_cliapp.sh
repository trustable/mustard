# Copyright (C) 2020  Codethink Limited
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 2 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# =*= License: GPL-2 =*=
#
# This is a bash script which attempts to install mustard's dependencies.
# It's mainly intended for ci and automated test setups...
#
# echo what we're doing
set -x
missing_deps=""
for dep in git python3 pip3; do
  ${dep} --version || missing_deps="${missing_deps} ${dep}"
done

if [ -n "${missing_deps}" ]; then
  echo "Unable to install cliapp missing dependencies:${missing_deps}"
  exit 1
fi

python3 -c "import cliapp"
if [ $? -ne 0 ]; then
  git clone git://git.liw.fi/cliapp/
  pip3 install -e cliapp
else
  echo "cliapp already installed"
  exit 1
fi

python3 -c "import cliapp"
if [ $? -eq 0 ]; then
  echo "cliapp installed"
  exit 0
else
  echo "cliapp install failed"
  exit 1
fi
