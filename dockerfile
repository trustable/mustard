FROM openjdk:15-jdk-buster

ARG REPO=https://gitlab.com/trustable/mustard.git
ARG BRANCH=master
WORKDIR /srv/mustard

RUN useradd -U -M mustard \
    && apt update \
    && apt-get install --assume-yes git bash python3 \
    && git clone --no-hardlinks $REPO mustard --branch $BRANCH \
    && cd mustard \
    && pwd \
    && ./install_dependencies.sh \
    && pip3 install -r requirements.txt \
    && mkdir mustard-mount

WORKDIR /srv/mustard/mustard

RUN chown -R mustard:mustard /srv/mustard

USER mustard

# Define container execution runtime
ENTRYPOINT ["./mustard-render", "-b", "-r", "-j", "plantuml.jar", "-p", "/srv/mustard/mustard/mustard-mount/"]
